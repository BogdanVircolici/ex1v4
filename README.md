JS:

function removeOrderItem(orderInfo, position){
    if(!Array.isArray(orderInfo.items)){
        throw new Error('Items should be an array')
    } else{
        for(let i=0;i< orderInfo.items.length;i++){
            if(!orderInfo.items[i].price || !orderInfo.items[i].quantity){
                        throw new Error('Malformed item')
            }
        } 
    }
    if(position>=orderInfo.items.length){
        throw new Error('Invalid position')
    }else{
        orderInfo.total=orderInfo.total-orderInfo.items[position].price*orderInfo.items[position].quantity;
        orderInfo.items.pop(position);
        return orderInfo;
    }
}

const app = {
    removeOrderItem
};

module.exports = app;